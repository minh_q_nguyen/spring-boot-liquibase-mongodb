package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import liquibase.exception.DatabaseException;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) throws DatabaseException {
		SpringApplication.run(DemoApplication.class, args);
	}

}
