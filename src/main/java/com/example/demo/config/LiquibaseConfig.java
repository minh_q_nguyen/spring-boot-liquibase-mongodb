package com.example.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import liquibase.Liquibase;
import liquibase.database.DatabaseFactory;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.ext.mongodb.database.MongoLiquibaseDatabase;
import liquibase.resource.ClassLoaderResourceAccessor;

@Configuration
public class LiquibaseConfig {

    @Value("${mongodb.host}")
    private String dbHost;

    @Value("${mongodb.database}")
    private String dbName;

    @Bean
    public ApplicationRunner executeDbChangeSets() {
        return args -> {
            MongoLiquibaseDatabase db;
            try {
                db = (MongoLiquibaseDatabase) DatabaseFactory.getInstance().openDatabase(
                    "mongodb://%s:27017/%s".formatted(dbHost, dbName), null, null, null, null, null);
            }
            catch (DatabaseException e) {
                System.err.println("Could not connect to database because: " + e.getMessage());
                System.exit(1);
                return;
            }

            try (var l = new Liquibase("db/changelog/master.xml", new ClassLoaderResourceAccessor(), db)) {
                l.update("");
            }
            catch (LiquibaseException e) {
                System.err.println("Could not run liquibase changesets because: " + e.getMessage());
                e.printStackTrace();
                System.exit(1);
            }
        };
    }

}
